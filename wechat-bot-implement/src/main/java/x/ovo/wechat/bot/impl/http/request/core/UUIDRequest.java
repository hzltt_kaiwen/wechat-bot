package x.ovo.wechat.bot.impl.http.request.core;

import org.dromara.hutool.core.net.url.UrlBuilder;
import x.ovo.wechat.bot.core.Constant;
import x.ovo.wechat.bot.core.http.request.ApiRequest;
import x.ovo.wechat.bot.core.util.RegexUtil;

import java.util.function.Function;
import java.util.regex.Pattern;

/**
 * 获取uuid请求
 *
 * @author ovo on 2024/07/15.
 */
public class UUIDRequest extends ApiRequest<String> {

    public static final Pattern UUID_PATTERN = Pattern.compile("window.QRLogin.code = (\\d+); window.QRLogin.uuid = \"(\\S+?)\";");

    @Override
    public ApiRequest<String> build() {
        String redirectUrl = UrlBuilder.of(Constant.Urls.NEWLOGINPAGE).addQuery("mod", "desktop").build();
        String url = UrlBuilder.of(Constant.Urls.LOGINJS)
                .addQuery("appid", Constant.APP_ID)
                .addQuery("fun", "new")
                .addQuery("lang", "zh_CN")
                .addQuery("redirect_uri", redirectUrl)
                .addQuery("_", System.currentTimeMillis())
                .build();
        return this.setUrl(url).setMethod(GET).setJson(false);
    }

    @Override
    protected Function<String, String> stringHandler() {
        return s -> RegexUtil.match(UUID_PATTERN, s, 2);
    }
}
