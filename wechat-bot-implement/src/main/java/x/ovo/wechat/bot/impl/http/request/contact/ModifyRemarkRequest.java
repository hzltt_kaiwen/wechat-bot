package x.ovo.wechat.bot.impl.http.request.contact;

import lombok.RequiredArgsConstructor;
import org.dromara.hutool.core.net.url.UrlBuilder;
import org.dromara.hutool.json.JSONUtil;
import x.ovo.wechat.bot.core.Constant;
import x.ovo.wechat.bot.core.http.request.ApiRequest;
import x.ovo.wechat.bot.core.http.result.ApiResult;

import java.util.function.Function;

@RequiredArgsConstructor
public class ModifyRemarkRequest extends ApiRequest<Boolean> {

    private final String remark;
    private final String username;

    @Override
    public ApiRequest<Boolean> build() {
        String s = UrlBuilder.of(this.session.getUrl())
                .addPath(Constant.Urls.MODIFY_CONTACT_REMARK)
                .build();
        return this.setUrl(s)
                .setMethod(POST)
                .setJson(true)
                .addParameter("CmdId", 2)
                .addParameter("BaseRequest", this.session.getBaseRequest())
                .addParameter("RemarkName", this.remark)
                .addParameter("UserName", this.username);
    }

    @Override
    protected Function<String, Boolean> stringHandler() {
        return s -> JSONUtil.toBean(s, ApiResult.class).success();
    }
}
