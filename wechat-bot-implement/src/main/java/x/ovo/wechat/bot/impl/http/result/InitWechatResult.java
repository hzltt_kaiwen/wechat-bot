package x.ovo.wechat.bot.impl.http.result;

import lombok.Data;
import lombok.EqualsAndHashCode;
import x.ovo.wechat.bot.core.entity.Contact;
import x.ovo.wechat.bot.core.http.result.ApiResult;
import x.ovo.wechat.bot.core.http.session.SyncKey;

import java.util.List;

/**
 * 初始化微信结果
 *
 * @author ovo on 2024/07/16.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class InitWechatResult extends ApiResult {

    private Integer Count;
    private List<Contact> ContactList;
    private SyncKey SyncKey;
    private Contact User;
    private String ChatSet;
    private String SKey;
    private Integer ClientVersion;
    private Integer SystemTime;
    private Integer GrayScale;
    private Integer InviteStartCount;
    private Integer ClickReportInterval;

}
