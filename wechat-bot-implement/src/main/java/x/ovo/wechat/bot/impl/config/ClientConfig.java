package x.ovo.wechat.bot.impl.config;

import lombok.Data;
import org.dromara.hutool.setting.yaml.YamlUtil;
import x.ovo.wechat.bot.core.Constant;

import java.util.Optional;

/**
 * 客户端配置类，用于存储和管理客户端的各种运行时配置选项。
 * 这些配置选项影响客户端的行为，例如自动登录、打印二维码、打印欢迎横幅、
 * 以及是否保存图像、语音和视频消息等。
 */
@Data
public class ClientConfig {

    /** 是否启用自动登录功能。 */
    private Boolean autoLogin = true;
    /** 是否打印二维码，用于扫描登录。 */
    private Boolean printQrCode = true;
    /** 是否在启动时打印欢迎横幅。 */
    private Boolean printBanner = true;
    /** 是否保存接收到的媒体数据（图片、语音、视频）。 */
    private Boolean saveMedia = false;
    /** 是否加密登录信息 */
    private Boolean encryptLoginInfo = true;
    /** 登录失败时的重试次数。 */
    private Integer reteyCount = 3;
    /** 心跳超时时间，单位为分，默认为1分钟，即上次心跳与监控线程检查时的时间间隔大于此值时，进程会结束 */
    private Integer heatbeatTimeout = 1;
    /** 保存登录信息时的加密密钥。 */
    private String encryptKey = System.getProperty("encryptKey", "wechat::bot");
    /** bot所有者 */
    private String owner = System.getProperty("owner", "");

    private static final ClientConfig INSTANCE;

    static {
        INSTANCE = Optional.ofNullable(load()).orElse(getDefault());
    }

    public static ClientConfig get() {
        return INSTANCE;
    }

    private static ClientConfig getDefault() {
        return new ClientConfig();
    }

    private static ClientConfig load() {
        if (!Constant.Files.CONFIG_FILE.exists()) return null;
        return YamlUtil.loadByPath(Constant.Files.CONFIG_FILE.getPath(), ClientConfig.class);
    }

}

