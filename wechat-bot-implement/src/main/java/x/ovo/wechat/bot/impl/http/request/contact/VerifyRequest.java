package x.ovo.wechat.bot.impl.http.request.contact;

import lombok.RequiredArgsConstructor;
import org.dromara.hutool.core.net.url.UrlBuilder;
import org.dromara.hutool.json.JSONObject;
import org.dromara.hutool.json.JSONUtil;
import x.ovo.wechat.bot.core.Constant;
import x.ovo.wechat.bot.core.entity.Recommend;
import x.ovo.wechat.bot.core.http.request.ApiRequest;
import x.ovo.wechat.bot.core.http.result.ApiResult;

import java.util.List;
import java.util.function.Function;

@RequiredArgsConstructor
public class VerifyRequest extends ApiRequest<Boolean> {

    private final Recommend recommend;

    @Override
    public ApiRequest<Boolean> build() {
        String s = UrlBuilder.of(this.session.getUrl())
                .addPath(Constant.Urls.VERIFY)
                .addQuery("r", System.currentTimeMillis())
                .addQuery("lang", "zh_CN")
                .addQuery("pass_ticket", this.session.getPassTicket())
                .build();
        JSONObject object = JSONUtil.ofObj().set("Value", recommend.getUserName()).set("VerifyUserTicket", recommend.getTicket());
        return this.setUrl(s)
                .setMethod(POST)
                .setJson(true)
                .addParameter("BaseRequest", this.baseRequest)
                .addParameter("Opcode", 3)
                .addParameter("SceneList", List.of(33))
                .addParameter("SceneListCount", 1)
                .addParameter("VerifyContent", "")
                .addParameter("VerifyUserList", List.of(object))
                .addParameter("VerifyUserListSize", 1)
                .addParameter("skey", this.session.getSKey());
    }

    @Override
    protected Function<String, Boolean> stringHandler() {
        return s -> JSONUtil.toBean(s, ApiResult.class).success();
    }
}
