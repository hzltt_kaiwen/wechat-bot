package x.ovo.wechat.bot.impl;

import x.ovo.wechat.bot.core.WechatClient;

public class Main {

    public static void main(String[] args) {
        WechatClient client = DefaultWechatClient.create();
        client.init();
        client.start();
    }
}
