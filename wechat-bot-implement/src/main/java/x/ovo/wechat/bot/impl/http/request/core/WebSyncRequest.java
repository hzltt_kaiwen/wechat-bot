package x.ovo.wechat.bot.impl.http.request.core;

import org.dromara.hutool.core.net.url.UrlBuilder;
import org.dromara.hutool.json.JSONUtil;
import x.ovo.wechat.bot.core.Constant;
import x.ovo.wechat.bot.core.http.request.ApiRequest;
import x.ovo.wechat.bot.core.http.result.WebSyncResult;

import java.util.function.Function;

public class WebSyncRequest extends ApiRequest<WebSyncResult> {
    @Override
    public ApiRequest<WebSyncResult> build() {
        String s = UrlBuilder.of(this.session.getUrl())
                .addPath(Constant.Urls.WEBWXSYNC)
                .addQuery("skey", this.session.getSKey())
                .addQuery("sid", this.session.getWxSid())
                .addQuery("pass_ticket", this.session.getPassTicket())
                .build();
        return this.setUrl(s)
                .setMethod(POST)
                .setJson(true)
                .addParameter("BaseRequest", this.session.getBaseRequest())
                .addParameter("SyncKey", this.session.getSyncKey())
                .addParameter("rr", ~(System.currentTimeMillis() / 1000));
    }

    @Override
    protected Function<String, WebSyncResult> stringHandler() {
        return s -> JSONUtil.toBean(s, WebSyncResult.class);
    }
}
