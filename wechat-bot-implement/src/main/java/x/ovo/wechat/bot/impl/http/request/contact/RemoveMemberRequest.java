package x.ovo.wechat.bot.impl.http.request.contact;

import lombok.RequiredArgsConstructor;
import org.dromara.hutool.core.net.url.UrlBuilder;
import org.dromara.hutool.json.JSONUtil;
import x.ovo.wechat.bot.core.Constant;
import x.ovo.wechat.bot.core.http.request.ApiRequest;
import x.ovo.wechat.bot.core.http.result.ApiResult;

import java.util.function.Function;

@RequiredArgsConstructor
public class RemoveMemberRequest extends ApiRequest<Boolean> {

    private final String group;
    private final String member;

    @Override
    public ApiRequest<Boolean> build() {
        String s = UrlBuilder.of(this.session.getUrl())
                .addPath(Constant.Urls.UPDATE_GROUP)
                .addQuery("fun", "delmember")
                .build();
        return this.setUrl(s)
                .setMethod(POST)
                .setJson(true)
                .addParameter("DelMemberList", this.member)
                .addParameter("ChatRoomName", this.group);
    }

    @Override
    protected Function<String, Boolean> stringHandler() {
        return s -> JSONUtil.toBean(s, ApiResult.class).success();
    }
}
