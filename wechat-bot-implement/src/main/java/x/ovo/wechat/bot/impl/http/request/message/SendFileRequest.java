package x.ovo.wechat.bot.impl.http.request.message;

import lombok.RequiredArgsConstructor;
import org.dromara.hutool.core.net.url.UrlBuilder;
import org.dromara.hutool.json.JSONUtil;
import x.ovo.wechat.bot.core.Constant;
import x.ovo.wechat.bot.core.entity.SendMessage;
import x.ovo.wechat.bot.core.http.request.ApiRequest;

import java.util.function.Function;

@RequiredArgsConstructor
public class SendFileRequest extends ApiRequest<Boolean> {

    private final SendMessage message;

    @Override
    public ApiRequest<Boolean> build() {
        String s = UrlBuilder.of(this.session.getUrl())
                .addPath(Constant.Urls.SEND_FILE)
                .addQuery("fun", "async")
                .addQuery("f", "json")
                .addQuery("lang", "zh_CN")
                .addQuery("pass_ticket", this.session.getPassTicket())
                .build();
        return this.setUrl(s)
                .setMethod(POST)
                .setJson(true)
                .addParameter("BaseRequest", this.session.getBaseRequest())
                .addParameter("Msg", this.message)
                .addParameter("Scene", 0);
    }

    @Override
    protected Function<String, Boolean> stringHandler() {
        return s -> JSONUtil.parseObj(s).<Integer>getByPath("BaseResponse.Ret", Integer.class) == 0;
    }
}
