package x.ovo.wechat.bot.impl.http.request.message;

import lombok.RequiredArgsConstructor;
import org.dromara.hutool.core.net.url.UrlBuilder;
import x.ovo.wechat.bot.core.Constant;
import x.ovo.wechat.bot.core.http.request.ApiRequest;

import java.util.function.Function;

@RequiredArgsConstructor
public class GetImageRequest extends ApiRequest<byte[]> {

    private final Long id;

    @Override
    public ApiRequest<byte[]> build() {
        String s = UrlBuilder.of(this.session.getUrl())
                .addPath(Constant.Urls.GET_MSG_IMG)
                .addQuery("MsgID", id)
                .addQuery("skey", this.session.getSKey())
                .build();
        return this.setUrl(s).setHandlerType(byte[].class);
    }

    @Override
    protected Function<byte[], byte[]> byteArrHandler() {
        return bytes -> bytes;
    }
}
