package x.ovo.wechat.bot.impl.http.request.contact;

import org.dromara.hutool.core.net.url.UrlBuilder;
import org.dromara.hutool.json.JSONObject;
import org.dromara.hutool.json.JSONUtil;
import x.ovo.wechat.bot.core.Constant;
import x.ovo.wechat.bot.core.entity.Contact;
import x.ovo.wechat.bot.core.http.request.ApiRequest;

import java.util.List;
import java.util.function.Function;

public class ListContactRequest extends ApiRequest<List<Contact>> {
    @Override
    public ApiRequest<List<Contact>> build() {
        String s = UrlBuilder.of(this.session.getUrl())
                .addPath(Constant.Urls.GET_CONTACT)
                .addQuery("r", System.currentTimeMillis())
                .addQuery("lang", "zh_CN")
                .addQuery("pass_ticket", this.session.getPassTicket())
                .addQuery("skey", this.session.getSKey())
                .addQuery("seq", 0)
                .build();
        return this.setUrl(s);
    }

    @Override
    protected Function<String, List<Contact>> stringHandler() {
        return s -> {
            JSONObject json = JSONUtil.parseObj(s);
            if (json.<Integer>getByPath("BaseResponse.Ret", Integer.class) == 0) {
                return json.getBeanList("MemberList", Contact.class);
            }
            return List.of();
        };
    }
}
