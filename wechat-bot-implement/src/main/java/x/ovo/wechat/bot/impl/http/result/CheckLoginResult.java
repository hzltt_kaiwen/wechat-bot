package x.ovo.wechat.bot.impl.http.result;

import lombok.Data;
import x.ovo.wechat.bot.core.enums.LoginMode;
import x.ovo.wechat.bot.core.util.RegexUtil;

import java.util.regex.Pattern;

@Data
public class CheckLoginResult {

    private static final Pattern CHECK_LOGIN_PATTERN = Pattern.compile("window.code=(\\d+)");
    private static final Pattern PROCESS_LOGIN_PATTERN = Pattern.compile("window.redirect_uri=\"(\\S+)\";");

    private LoginMode loginMode;
    private String url;

    /**
     * 根据响应信息创建
     * <p> window.code=xxx;window.redirect_uri="https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxnewloginpage?ticket=xxx&uuid=xxx&lang=xxx&scan=xxx";
     *
     * @param data 数据
     * @return {@link CheckLoginResult}
     */
    public static CheckLoginResult from(String data) {
        CheckLoginResult result = new CheckLoginResult();
        result.setLoginMode(LoginMode.get(Integer.parseInt(RegexUtil.match(CHECK_LOGIN_PATTERN, data))));
        result.setUrl(RegexUtil.match(PROCESS_LOGIN_PATTERN, data));
        return result;
    }
}
