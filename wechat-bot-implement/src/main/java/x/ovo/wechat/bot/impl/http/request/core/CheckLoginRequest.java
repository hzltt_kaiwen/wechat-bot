package x.ovo.wechat.bot.impl.http.request.core;

import lombok.RequiredArgsConstructor;
import org.dromara.hutool.core.net.url.UrlBuilder;
import x.ovo.wechat.bot.core.Constant;
import x.ovo.wechat.bot.core.http.request.ApiRequest;
import x.ovo.wechat.bot.impl.http.result.CheckLoginResult;

import java.util.function.Function;

/**
 * 检查登录请求
 *
 * @author ovo on 2024/07/16.
 */
@RequiredArgsConstructor
public class CheckLoginRequest extends ApiRequest<CheckLoginResult> {

    private final int tip;
    private final String uuid;

    @Override
    public ApiRequest<CheckLoginResult> build() {
        String string = UrlBuilder.of(Constant.Urls.LOGIN)
                .addQuery("loginicon", true)
                .addQuery("tip", this.tip)
                .addQuery("uuid", this.uuid)
                .addQuery("r", -System.currentTimeMillis() / 1000 / 1579)
                .addQuery("_", System.currentTimeMillis())
                .build();
        return this.setUrl(string).addHeader("Referer", "https://wx.qq.com/");
    }

    @Override
    protected Function<String, CheckLoginResult> stringHandler() {
        return CheckLoginResult::from;
    }
}
