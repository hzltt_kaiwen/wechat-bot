package x.ovo.wechat.bot.impl.http.request.core;

import org.dromara.hutool.core.net.url.UrlBuilder;
import org.dromara.hutool.json.JSONObject;
import org.dromara.hutool.json.JSONUtil;
import x.ovo.wechat.bot.core.Constant;
import x.ovo.wechat.bot.core.http.request.ApiRequest;

import java.util.function.Function;

/**
 * 推送登录请求
 *
 * @author ovo on 2024/07/16.
 */
public class PushLoginRequest extends ApiRequest<String> {
    @Override
    public ApiRequest<String> build() {
        this.url = UrlBuilder.of(this.session.getUrl())
                .addPath(Constant.Urls.PUSH_LOGIN)
                .addQuery("uin", this.session.getWxUin())
                .build();
        return this;
    }

    @Override
    protected Function<String, String> stringHandler() {
        return s -> {
            JSONObject obj = JSONUtil.parseObj(s);
            return obj.getInt("ret") == 0 ? obj.getStr("uuid") : null;
        };
    }
}
