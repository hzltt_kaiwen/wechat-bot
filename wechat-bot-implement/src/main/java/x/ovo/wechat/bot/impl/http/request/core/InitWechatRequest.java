package x.ovo.wechat.bot.impl.http.request.core;

import org.dromara.hutool.core.net.url.UrlBuilder;
import org.dromara.hutool.json.JSONUtil;
import x.ovo.wechat.bot.core.Constant;
import x.ovo.wechat.bot.core.http.request.ApiRequest;
import x.ovo.wechat.bot.impl.http.result.InitWechatResult;

import java.util.function.Function;

public class InitWechatRequest extends ApiRequest<InitWechatResult> {
    @Override
    public ApiRequest<InitWechatResult> build() {
        String s = UrlBuilder.of(this.session.getUrl())
                .addPath(Constant.Urls.WXINIT)
                .addQuery("pass_ticket", this.session.getPassTicket())
                .addQuery("r", System.currentTimeMillis())
                .build();
        return this.setUrl(s)
                .setMethod(POST)
                .setJson(true)
                .addParameter("BaseRequest", this.baseRequest);
    }

    @Override
    protected Function<String, InitWechatResult> stringHandler() {
        return s -> JSONUtil.toBean(s, InitWechatResult.class);
    }
}
