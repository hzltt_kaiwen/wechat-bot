package x.ovo.wechat.bot.impl.http.request.core;

import org.dromara.hutool.core.net.url.UrlBuilder;
import x.ovo.wechat.bot.core.Constant;
import x.ovo.wechat.bot.core.http.request.ApiRequest;

import java.util.function.Function;

public class LogoutRequest extends ApiRequest<Void> {
    @Override
    public ApiRequest<Void> build() {
        String s = UrlBuilder.of(this.session.getUrl())
                .addPath(Constant.Urls.LOGOUT)
                .addQuery("redirect", 1)
                .addQuery("type", 0)
                .addQuery("skey", this.session.getSKey())
                .build();
        return this.setUrl(s)
                .addParameter("uin", this.session.getWxUin())
                .addParameter("sid", this.session.getWxSid());
    }

    @Override
    protected Function<String, Void> stringHandler() {
        return null;
    }
}
