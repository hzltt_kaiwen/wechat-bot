package x.ovo.wechat.bot.impl.contact;

import x.ovo.wechat.bot.core.contact.Contactable;
import x.ovo.wechat.bot.core.contact.RetrievalStratrgy;
import x.ovo.wechat.bot.core.contact.RetrievalType;

import java.util.Iterator;

public class RemarkStrategy extends RetrievalStratrgy {
    @Override
    public RetrievalType getType() {
        return RetrievalType.REMARK_NAME;
    }

    @Override
    public Contactable get(Iterator<? extends Contactable> iterator, String key) {
        while (iterator.hasNext()) {
            Contactable contact = iterator.next();
            if (contact.getRemarkName().equals(key)) {
                return contact;
            }
        }
        return null;
    }

    @Override
    public boolean remove(Iterator<? extends Contactable> iterator, String key) {
        while (iterator.hasNext()) {
            Contactable contact = iterator.next();
            if (contact.getRemarkName().equals(key)) {
                iterator.remove();
                return true;
            }
        }
        return false;
    }
}
