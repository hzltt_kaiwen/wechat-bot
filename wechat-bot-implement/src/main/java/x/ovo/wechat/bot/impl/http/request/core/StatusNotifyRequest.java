package x.ovo.wechat.bot.impl.http.request.core;

import lombok.RequiredArgsConstructor;
import org.dromara.hutool.core.net.url.UrlBuilder;
import org.dromara.hutool.json.JSONUtil;
import x.ovo.wechat.bot.core.Constant;
import x.ovo.wechat.bot.core.http.request.ApiRequest;

import java.util.Objects;
import java.util.function.Function;

@RequiredArgsConstructor
public class StatusNotifyRequest extends ApiRequest<Boolean> {

    private final String id;

    @Override
    public ApiRequest<Boolean> build() {
        String s = UrlBuilder.of(this.session.getUrl())
                .addPath(Constant.Urls.STATUS_NOTIFY)
                .addQuery("lang", "zh_CN")
                .addQuery("pass_ticket", this.session.getPassTicket())
                .build();
        return this.setUrl(s)
                .addParameter("BaseRequest", this.session.getBaseRequest())
                .addParameter("ClientMsgId", System.currentTimeMillis())
                .addParameter("Code", Objects.isNull(this.id) ? 3 : 1)
                .addParameter("FromUserName", this.session.getUserName())
                .addParameter("ToUserName", Objects.isNull(this.id) ? this.session.getUserName() : this.id);
    }

    @Override
    protected Function<String, Boolean> stringHandler() {
        return s -> JSONUtil.parseObj(s).<Integer>getByPath("BaseResponse.Ret", Integer.class) == 0;
    }
}
