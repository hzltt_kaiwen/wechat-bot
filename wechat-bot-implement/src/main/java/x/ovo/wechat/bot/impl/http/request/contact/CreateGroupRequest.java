package x.ovo.wechat.bot.impl.http.request.contact;

import lombok.RequiredArgsConstructor;
import org.dromara.hutool.core.net.url.UrlBuilder;
import org.dromara.hutool.json.JSONObject;
import org.dromara.hutool.json.JSONUtil;
import x.ovo.wechat.bot.core.Constant;
import x.ovo.wechat.bot.core.http.request.ApiRequest;
import x.ovo.wechat.bot.core.http.result.ApiResult;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;

@RequiredArgsConstructor
public class CreateGroupRequest extends ApiRequest<Boolean> {

    private final String groupName;
    private final Collection<String> members;

    @Override
    public ApiRequest<Boolean> build() {
        String s = UrlBuilder.of(this.session.getUrl())
                .addPath(Constant.Urls.CREATE_GROUP)
                .addQuery("lang", "zh_CN")
                .addQuery("r", System.currentTimeMillis())
                .build();
        List<JSONObject> list = this.members.stream()
                .map(e -> JSONUtil.ofObj().set("UserName", e).set("EncryChatRoomId", ""))
                .toList();
        return this.setUrl(s)
                .setMethod(POST)
                .setJson(true)
                .addParameter("BaseRequest", this.session.getBaseRequest())
                .addParameter("MemberCount", members.size())
                .addParameter("MemberList", list)
                .addParameter("Topic", groupName);
    }

    @Override
    protected Function<String, Boolean> stringHandler() {
        return s -> JSONUtil.toBean(s, ApiResult.class).success();
    }
}
