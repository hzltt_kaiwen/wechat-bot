package x.ovo.wechat.bot.plugin;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import org.dromara.hutool.core.collection.CollUtil;
import org.dromara.hutool.core.reflect.ClassUtil;
import org.dromara.hutool.core.text.StrUtil;
import x.ovo.wechat.bot.core.event.Event;
import x.ovo.wechat.bot.core.event.EventListener;
import x.ovo.wechat.bot.core.event.ExceptionEvent;
import x.ovo.wechat.bot.core.event.SystemEvent;
import x.ovo.wechat.bot.core.plugin.Plugin;
import x.ovo.wechat.bot.plugin.push.DingDingPushStrategy;
import x.ovo.wechat.bot.plugin.push.PushContext;
import x.ovo.wechat.bot.plugin.push.TelegramPushStrategy;
import x.ovo.wechat.bot.plugin.push.FeiShuPushStrategy;
import x.ovo.wechat.bot.plugin.push.PushPlusPushStrategy;

@Slf4j
public class PushListener extends EventListener<Event<Object>, Object> {

    private final Map<String, ?> config = this.plugin.getConfig();
    private final OkHttpClient client = (OkHttpClient) this.context.getHttpEngine().getRawEngine();
    private final PushContext pushContext = new PushContext(); // 添加推送上下文

    public PushListener(Plugin plugin) {
        super(plugin);
    }

    @Override
    public boolean support(@NonNull Event event, Object source) {
        return event instanceof SystemEvent || event instanceof ExceptionEvent;
    }

    @SneakyThrows
    @Override
    public boolean onEvent(@NonNull Event event, Object source) {
        // 检查关键词黑名单
        String pushKeyblacklist = (String) config.get("push_keyblacklist");
        if (StrUtil.isBlank(pushKeyblacklist)) {
            log.info("push未找到推送配置“push_keyblacklist”，跳过关键词黑名单检查");
        } else {
            log.info("push关键词黑名单为： {}", pushKeyblacklist);
            String[] pushKeyblacklists = pushKeyblacklist.split(",");
            for (String Keyblacklist : pushKeyblacklists) {
                String eventmsg = event.getSource().toString();
                if (eventmsg.contains(Keyblacklist)) return false;
            }
        }

        // 检查推送事件
        String className = ClassUtil.getClassName(event, true);
        String pushEvent = (String) config.get("push_event");
        if (StrUtil.isBlank(pushEvent)) {
            log.info("push未找到推送配置“push_event”，默认推送全部事件");
            pushEvent = "ExceptionEvent,SystemEvent,LoginSystemEvent,LogoutSystemEvent";
        } else {
            log.info("push推送事件为： {}", pushEvent);
        }
        String[] pushEvents = pushEvent.split(",");
        List<String> pushEventslist = Arrays.asList(pushEvents);
        if (!CollUtil.contains(pushEventslist, className)) return false;

        // 根据配置选择推送策略
        String pushChannel = (String) config.get("push_channel");
        if (StrUtil.isBlank(pushChannel)) {
            log.info("push未找到推送配置“push_channel”，默认使用全部策略推送");
            pushChannel = "dingding,feishu,pushplus,telegram";
        } else {
            log.info("push推送策略为： {}", pushChannel);
        }
        String[] pushChannels = pushChannel.split(",");
        for (String pushway : pushChannels) {
            switch (pushway) {
                case "dingding" -> pushContext.setStrategy(new DingDingPushStrategy());
                case "telegram" -> pushContext.setStrategy(new TelegramPushStrategy());
                case "feishu" -> pushContext.setStrategy(new FeiShuPushStrategy());
                case "pushplus" -> pushContext.setStrategy(new PushPlusPushStrategy());
            }
            if (event instanceof ExceptionEvent && source instanceof Throwable e) {
                String error = LogUtil.printError(e);
                pushContext.push(config, client, "异常消息", error);
            } else if (event instanceof SystemEvent se) {
                pushContext.push(config, client, "系统消息", se.getSource());
            }
        }
        return true;
    }

    @Override
    public boolean executeNext() {
        return true;
    }
}