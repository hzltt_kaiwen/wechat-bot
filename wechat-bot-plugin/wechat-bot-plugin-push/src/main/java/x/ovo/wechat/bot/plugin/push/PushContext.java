package x.ovo.wechat.bot.plugin.push;

import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;

/**
 * Author: wxy97.com
 * Date: 2024/9/30 11:30
 * Description:
 */
@Slf4j
public class PushContext {

    private PushStrategy strategy;

    public void setStrategy(PushStrategy strategy) {
        this.strategy = strategy;
    }

    public void push(Map<String, ?> config, OkHttpClient client, String flag, String content) {
        if (strategy != null) {
            strategy.push(config, client, flag, content);
        } else {
            log.error("没有定义推送策略");
        }
    }
}
