package x.ovo.wechat.bot.plugin.push;


import java.util.Map;
import okhttp3.OkHttpClient;

/**
 * Author: wxy97.com
 * Date: 2024/9/30 11:29
 * Description:
 */
public interface PushStrategy {
    void push(Map<String, ?> config, OkHttpClient client, String flag, String content);
}
