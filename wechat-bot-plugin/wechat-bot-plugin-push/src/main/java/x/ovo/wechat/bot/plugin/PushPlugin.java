package x.ovo.wechat.bot.plugin;

import lombok.extern.slf4j.Slf4j;
import org.dromara.hutool.core.io.file.FileUtil;
import x.ovo.wechat.bot.core.Constant;
import x.ovo.wechat.bot.core.command.CommandExcutor;
import x.ovo.wechat.bot.core.event.EventListener;
import x.ovo.wechat.bot.core.plugin.Plugin;

/**
 * 推送插件
 *
 * @author ovo, created by 2024/07/13
 */
@Slf4j(topic = "PushPlugin")
public class PushPlugin extends Plugin {
    @Override
    public void onLoad() {
        this.reloadConfig();
        if (!FileUtil.file(this.getDataDir(), Constant.Files.CONFIG_FILE_NAME).exists()) {
            this.saveDefaultConfig();
        }
    }

    @Override
    public EventListener<?, ?> getEventListener() {
        return new PushListener(this);
    }

    @Override
    public CommandExcutor getCommandExcutor() {
        return null;
    }
}
