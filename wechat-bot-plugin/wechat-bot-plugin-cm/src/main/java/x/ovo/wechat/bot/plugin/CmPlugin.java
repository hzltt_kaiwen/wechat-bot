package x.ovo.wechat.bot.plugin;

import x.ovo.wechat.bot.core.command.CommandExcutor;
import x.ovo.wechat.bot.core.event.EventListener;
import x.ovo.wechat.bot.core.plugin.Plugin;

public class CmPlugin extends Plugin {
    @Override
    public void onLoad() {

    }

    @Override
    public EventListener<?, ?> getEventListener() {
        return null;
    }

    @Override
    public CommandExcutor getCommandExcutor() {
        return new CmExecutor(this);
    }
}
