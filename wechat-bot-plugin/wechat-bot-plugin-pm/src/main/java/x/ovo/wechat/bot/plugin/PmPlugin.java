package x.ovo.wechat.bot.plugin;

import x.ovo.wechat.bot.core.command.CommandExcutor;
import x.ovo.wechat.bot.core.event.EventListener;
import x.ovo.wechat.bot.core.plugin.Plugin;

/**
 * 插件管理器插件
 *
 * @author ovo on 2024/07/24.
 */
public class PmPlugin extends Plugin {
    @Override
    public void onLoad() {

    }

    @Override
    public EventListener<?, ?> getEventListener() {
        return null;
    }

    @Override
    public CommandExcutor getCommandExcutor() {
        return new PmExecutor(this);
    }
}
