package x.ovo.wechat.bot.core.entity;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * 发送消息体
 *
 * @author ovo on 2024/04/19.
 */
@Data
@Builder
public class SendMessage implements Serializable {

    /** 类型 */
    private Integer Type;
    /** 内容 */
    private String Content;
    /** 表情符号 MD5 */
    private String EMoticonMd5;
    /** 发送用户名 */
    private String FromUserName;
    /** 接收用户名 */
    private String ToUserName;
    /** 本地 ID */
    private String LocalID;
    /** 客户端消息 ID */
    private String ClientMsgId;
    /** 媒体 ID */
    private String MediaId;

}
