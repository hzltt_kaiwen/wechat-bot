package x.ovo.wechat.bot.core.exception;

import lombok.experimental.StandardException;

/**
 * 登录异常
 *
 * @author ovo, created by 2024/07/12
 */
@StandardException
public class LoginException extends RuntimeException{
}
