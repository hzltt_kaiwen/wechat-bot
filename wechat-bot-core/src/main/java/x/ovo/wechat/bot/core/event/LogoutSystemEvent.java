package x.ovo.wechat.bot.core.event;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 注销系统事件
 *
 * @author ovo, created by 2024/07/06
 */
public class LogoutSystemEvent extends SystemEvent{
    public LogoutSystemEvent(String uin) {
        super(String.format("[logout] 机器人 [%s] 于 %s 退出登录", uin, LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))));
    }
}
