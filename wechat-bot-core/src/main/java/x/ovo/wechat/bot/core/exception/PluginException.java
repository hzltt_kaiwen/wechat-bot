package x.ovo.wechat.bot.core.exception;

import lombok.experimental.StandardException;

/**
 * 插件异常
 *
 * @author ovo, created by 2024/07/06
 */
@StandardException
public class PluginException extends RuntimeException {

    /** 未找到 YAML */
    public static final PluginException NOT_FOUND_YAML = new PluginException("尝试加载的插件不存在 plugin.y(a)ml，无法获得插件描述信息");
    /** 未找到主类 */
    public static final PluginException NOT_FOUND_MAIN = new PluginException("插件主类必须继承 Plugin");

}
