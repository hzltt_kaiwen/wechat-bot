package x.ovo.wechat.bot.core.http.request;

import lombok.Data;

import java.io.Serializable;

/**
 * 基础请求信息类，用于封装微信网页版聊天机器人登录所需的会话信息。
 * <p>
 * 该类实现了Serializable接口，以便对象可以在序列化和反序列化过程中被正确处理。
 *
 * @author ovo, created by 2024/07/06
 */
@Data
public class BaseRequest implements Serializable {
    /**
     * 会话标识Skey，用于标识一次会话。
     * <p>
     * Skey是服务器端生成的，用于验证客户端请求的合法性。
     */
    private String Skey;

    /**
     * 会话ID(Sid)，用于标识当前的会话。
     * <p>
     * Sid在登录过程中由服务器生成，并在后续的请求中用于验证用户身份。
     */
    private String Sid;

    /**
     * 用户的Uin（用户标识），用于唯一标识一个微信用户。
     * <p>
     * Uin是微信服务器为每个用户分配的唯一标识符。
     */
    private String Uin;

    /**
     * 设备ID(DeviceID)，用于标识用户使用的设备。
     * <p>
     * 设备ID通常由客户端生成，用于区分不同的设备或会话。
     */
    private String DeviceID;

    /**
     * 临时数据票据(dataTicket)，用于临时存储一些会话相关的数据。
     * <p>
     * 该字段是临时的，不会被序列化存储。
     */
    private transient String dataTicket;

    /**
     * 登录票据(passTicket)，用于验证用户登录状态。
     * <p>
     * passTicket是在登录过程中生成的，用于验证用户是否已成功登录。
     * 该字段是临时的，不会被序列化存储。
     */
    private transient String passTicket;

    /**
     * 验证票据(authTicket)，用于进行身份验证。
     * <p>
     * authTicket通常在登录过程中生成，用于验证用户的身份。
     * 该字段是临时的，不会被序列化存储。
     */
    private transient String authTicket;

}
