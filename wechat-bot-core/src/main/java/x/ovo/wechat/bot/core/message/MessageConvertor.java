package x.ovo.wechat.bot.core.message;

/**
 * 消息转换器
 * <p>
 * 将微信原始消息转换为业务消息
 *
 * @author ovo on 2024/07/18.
 */
@FunctionalInterface
public interface MessageConvertor {
    Message convert(RawMessage rawMessage);
}
