package x.ovo.wechat.bot.core.message;

import lombok.Data;
import x.ovo.wechat.bot.core.Constant;
import x.ovo.wechat.bot.core.contact.Contactable;
import x.ovo.wechat.bot.core.entity.Member;
import x.ovo.wechat.bot.core.enums.MessageType;

import java.io.Serializable;

/**
 * 封装了原始消息、携带更多数据的消息对象
 *
 * @author ovo, created by 2024/07/06
 */
@Data
public class Message implements Serializable {

    /** 微信原始消息 */
    protected RawMessage raw;

    /** mssage_id */
    protected Long id;
    /** 文本内容 */
    protected String content;
    /** 消息类型 */
    protected MessageType type;
    /** 来自用户 */
    protected Contactable from;
    /** 到用户 */
    protected Contactable to;
    /** 群消息发送者 */
    protected Member member;

    /**
     * 此消息是否为群聊消息
     *
     * @return boolean
     */
    public boolean isGroup() {
        return this.from.getUserName().contains(Constant.GROUP_IDENTIFY) || this.to.getUserName().contains(Constant.GROUP_IDENTIFY);

    }
}
