package x.ovo.wechat.bot.core.util;

import com.vdurmont.emoji.EmojiParser;
import lombok.experimental.UtilityClass;
import x.ovo.wechat.bot.core.Constant;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@UtilityClass
public class WechatUtil {

    // emoji正则
    private static final Pattern PATTERN_EMOJI = Pattern.compile("<span class=\"emoji emoji(.{1,10})\"></span>");


    /**
     * 生成设备ID。
     * <p>
     * 设备ID是以“e”为前缀的15位数字字符串
     *
     * @return 生成的设备ID字符串。
     */
    public static String generateDeviceId() {
        long rand = new Random().nextLong() & 0x7FFFFFFFFFFFFFFFL;
        return "e" + String.format("%015d", rand);
    }

    /**
     * 格式化字符串，处理emoji表情、HTML转义符、Unicode控制符
     *
     * @param str str
     * @return {@link String }
     */
    public static String format(String str) {
        if (str == null || str.isEmpty()) return "";
        str = str.replaceAll("[\\p{C}\\p{Zl}\\p{Zp}]", "")
                .replaceAll("<br/>", "\n")
                .replaceAll("\\u2005", " ")
                .replaceAll("&lt;", "<")
                .replaceAll("&gt;", ">")
                .replaceAll("&amp;", "&")
                .replaceAll("&quot;", "\"")
                .replaceAll("&apos;", "'")
                .replaceAll("&nbsp;", " ");
        return parseEmoji(str);
    }

    /**
     * 处理emoji表情
     *
     * @param text 发短信
     * @return {@link String }
     */
    private static String parseEmoji(String text) {
        // 检车输入文字是否为空或null
        if (text == null || text.isEmpty() || text.isBlank()) return text;
        // 检查文本是否包含emoji
        if (!text.contains("<span class=\"emoji emoji")) return text;

        Matcher m = PATTERN_EMOJI.matcher(text);
        StringBuilder sb = new StringBuilder();
        int lastStart = 0;
        while (m.find()) {
            String str = m.group(1);
            if (str.length() != 6 && str.length() != 10) {
                str = "&#x" + str + ";";
                String tmp = text.substring(lastStart, m.start());
                sb.append(tmp).append(str);
                lastStart = m.end();
            }
        }
        if (lastStart < text.length()) {
            sb.append(text.substring(lastStart));
        }
        return EmojiParser.parseToUnicode(sb.toString());
    }


    /**
     * 在控制台打印此项目相关信息
     */
    public static void printInfo() {
        String banner = """

                                                    ,---,                   ___                                      ___
                                                  ,--.' |                 ,--.'|_              ,---,               ,--.'|_
                         .---.                    |  |  :                 |  | :,'     ,---,.,---.'|      ,---.    |  | :,'
                        /. ./|                    :  :  :                 :  : ' :   ,'  .' ||   | :     '   ,'\\   :  : ' :
                     .-'-. ' |   ,---.     ,---.  :  |  |,--.  ,--.--.  .;__,'  /  ,---.'   ,:   : :    /   /   |.;__,'  /
                    /___/ \\: |  /     \\   /     \\ |  :  '   | /       \\ |  |   |   |   |    |:     |,-..   ; ,. :|  |   |
                 .-'.. '   ' . /    /  | /    / ' |  |   /' :.--.  .-. |:__,'| :   :   :  .' |   : '  |'   | |: ::__,'| :
                /___/ \\:     '.    ' / |.    ' /  '  :  | | | \\__\\/: . .  '  : |__ :   |.'   |   |  / :'   | .; :  '  : |__
                .   \\  ' .\\   '   ;   /|'   ; :__ |  |  ' | : ," .--.; |  |  | '.'|`---'     '   : |: ||   :    |  |  | '.'|
                 \\   \\   ' \\ |'   |  / |'   | '.'||  :  :_:,'/  /  ,.  |  ;  :    ;          |   | '/ : \\   \\  /   ;  :    ;
                  \\   \\  |--" |   :    ||   :    :|  | ,'   ;  :   .'   \\ |  ,   /           |   :    |  `----'    |  ,   /
                   \\   \\ |     \\   \\  /  \\   \\  / `--''     |  ,     .-./  ---`-'            /    \\  /              ---`-'
                    '---"       `----'    `----'             `--`---'                        `-'----'
                """;
        System.out.println(banner);
        System.out.println("                                                                version: " + Constant.VERSION);
        System.out.println("                                                                author: ovo");
        System.out.println("                                                                email: merlin9710@gmail.com\n");
    }

}
