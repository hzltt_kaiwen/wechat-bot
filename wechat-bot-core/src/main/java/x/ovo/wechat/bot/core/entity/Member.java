package x.ovo.wechat.bot.core.entity;

import lombok.Data;
import x.ovo.wechat.bot.core.contact.Contactable;
import x.ovo.wechat.bot.core.util.WechatUtil;

/**
 * 群成员
 *
 * @author ovo, created by 2024/07/06
 */
@Data
public class Member implements Contactable {

    private Long Uin;
    /** 用户唯一标识 */
    private String UserName;
    /** 微信昵称 */
    private String NickName;
    /** 备注名 */
    private String RemarkName;
    /** 显示名称 */
    private String DisplayName;
    /** 成员状态 */
    private Integer MemberStatus;
    /** 关键词 */
    private String KeyWord;
    /** ATTR 状态 */
    private Long AttrStatus;

    @Override
    public Integer getVerifyFlag() {
        return 0;
    }

    @Override
    public void format() {
        this.NickName = WechatUtil.format(this.NickName);
        this.RemarkName = WechatUtil.format(this.RemarkName);
        this.DisplayName = WechatUtil.format(this.DisplayName);
    }
}
