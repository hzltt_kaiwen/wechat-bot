package x.ovo.wechat.bot.core;

/**
 * 微信客户端接口
 *
 * @author ovo on 2024/07/10.
 */
public interface WechatClient {

    void init();

    void start();

    void stop();

    void mkdirs();

    void saveDefaultConfig();
}
