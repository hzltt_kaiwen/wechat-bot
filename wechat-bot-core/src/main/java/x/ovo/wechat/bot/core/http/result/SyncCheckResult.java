package x.ovo.wechat.bot.core.http.result;

import lombok.Data;
import x.ovo.wechat.bot.core.enums.SyncCheck;

import java.io.Serializable;

/**
 * 同步检查结果类，用于封装同步检查操作的结果信息。
 * <p>
 * 实现Serializable接口，以便结果对象可以被序列化，用于跨网络传输或持久化存储。
 *
 * @author ovo, created by 2024/07/07
 */
@Data
public class SyncCheckResult implements Serializable {

    /** 结果代码，用于表示同步检查操作的结果状态。 */
    private SyncCheck.RetCode retCode;

    /** 选择器，用于指示同步检查操作中涉及的具体对象或条件。 */
    private SyncCheck.Selector selector;

    /**
     * 构造函数，根据给定的代码和选择器初始化SyncCheckResult对象。
     *
     * @param retCode  操作结果代码，映射为SyncCheck.RetCode枚举值。
     * @param selector 操作选择器，映射为SyncCheck.Selector枚举值。
     */
    public SyncCheckResult(Integer retCode, Integer selector) {
        this.retCode = SyncCheck.RetCode.get(retCode);
        this.selector = SyncCheck.Selector.get(selector);
    }
}

