package x.ovo.wechat.bot.core.exception;

import lombok.experimental.StandardException;

@StandardException
public class YamlException extends RuntimeException{
}
