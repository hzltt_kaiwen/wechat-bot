package x.ovo.wechat.bot.core.plugin;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 插件说明
 *
 * @author ovo, created by 2024/07/06
 */
@Data
public class PluginDescription implements Serializable {

    /** 插件名称 */
    private String name;
    /** 插件主类 */
    private String main;
    /** 版本 */
    private String version;
    /** 优先权 */
    private Integer priority;
    /** 描述 */
    private String description;
    /** 作者 */
    private List<String> authors;

}
