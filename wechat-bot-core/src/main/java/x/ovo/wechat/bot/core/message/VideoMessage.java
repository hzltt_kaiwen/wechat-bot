package x.ovo.wechat.bot.core.message;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 视频消息
 *
 * @author ovo, created by 2024/07/06
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class VideoMessage extends Message {

    /** 视频字节数组 */
    private byte[] bytes;
    /** 长度 */
    private long length;

    @Override
    public String getContent() {
        return String.format("[视频] %d s", this.length);
    }
}
