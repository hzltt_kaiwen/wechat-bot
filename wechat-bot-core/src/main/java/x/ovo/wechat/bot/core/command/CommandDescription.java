package x.ovo.wechat.bot.core.command;

import lombok.Data;

import java.util.Set;

/**
 * 命令说明
 *
 * @author ovo, created by 2024/07/06
 */
@Data
public class CommandDescription {

    /** 命令 */
    private String command;
    /** 描述 */
    private String description;
    /** 别名 */
    private Set<String> aliases;
    /** 有权限执行的用户名 */
    private Set<String> users;
    /** 无权限提示 */
    private String tip;
    /** 用法 */
    private String usage;

}
