package x.ovo.wechat.bot.core.http.result;

import lombok.Data;

import java.io.Serializable;

/**
 * 基础响应类，用于封装网页版微信接口调用的响应结果。
 *
 * @author ovo, created by 2024/07/06
 */
@Data
public class BaseResponse implements Serializable {

    /** 错误码，0表示成功 */
    private Integer Ret;
    /** 错误信息，对错误的详细描述 */
    private String ErrMsg;

}
