package x.ovo.wechat.bot.core.exception;

import lombok.experimental.StandardException;

@StandardException
public class CommandException extends RuntimeException {

    public static CommandException parameterError = new CommandException("参数错误");

}
