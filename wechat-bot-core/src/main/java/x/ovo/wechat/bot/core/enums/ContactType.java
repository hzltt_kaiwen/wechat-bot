package x.ovo.wechat.bot.core.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import x.ovo.wechat.bot.core.contact.Contactable;

/**
 * 联系人账户类型
 *
 * @author ovo, created by 2024/07/06
 */
@Getter
@RequiredArgsConstructor
public enum ContactType {

    OFFICIAL_ACCOUNT("公众号"),
    FRIEND("好友"),
    GROUP("群聊"),
    SPECIAL("特殊账号"),
    PERSONAL("个人"),
    ;
    private final String name;

    public static ContactType get(Contactable contact) {
        if (contact.getUserName().startsWith("@@")) return GROUP;
        if (contact.getUserName().startsWith("@")) return FRIEND;
        if (contact.getVerifyFlag() == 8 || contact.getVerifyFlag() == 24 || contact.getVerifyFlag() == 136)
            return OFFICIAL_ACCOUNT;
        return PERSONAL;
    }

}
