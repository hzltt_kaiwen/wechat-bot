package x.ovo.wechat.bot.core.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import x.ovo.wechat.bot.core.Context;
import x.ovo.wechat.bot.core.Manager;
import x.ovo.wechat.bot.core.message.TextMessage;
import x.ovo.wechat.bot.core.plugin.Plugin;

import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * 命令管理器
 *
 * @author ovo, created by 2024/07/06
 */
public interface CommandManager extends Manager {

    Logger log = LoggerFactory.getLogger(CommandManager.class.getSimpleName());

    /**
     * 注册插件中的命令执行器。
     *
     * @param plugin 命令所属的插件。
     */
    void register(Plugin plugin);

    /**
     * 取消注册一个命令。
     *
     * @param command 要取消注册的命令的标识符。
     */
    void unregister(String command);

    /**
     * 执行一个文本消息中包含的命令。
     *
     * @param message 包含命令的文本消息。
     *               该方法将解析文本消息中的命令，并调用相应的命令执行器来处理命令。
     */
    void execute(TextMessage message);

    /**
     * 列表
     *
     * @return {@link List }<{@link CommandExcutor }>
     */
    Set<CommandExcutor> list();

    /**
     * 检查用户是否具有执行该命令的权限。
     *
     * @param command 命令
     * @param user    用户昵称
     * @return boolean
     */
    boolean hasPermission(String command, String user);

    /**
     * 添加命令权限
     *
     * @param command 命令
     * @param user    用户
     */
    void addPermission(String command, String user);

    /**
     * 删除权限
     *
     * @param command 命令
     * @param user    用户
     */
    void removePermission(String command, String user);

    /**
     * 获取命令和具有执行权限用户的map
     *
     * @return {@link Map }<{@link String }, {@link Set }<{@link String }>>
     */
    Map<String, Set<String>> getPermissions();

    /**
     * 获取具有权限执行命令的用户集合
     *
     * @param command 命令
     * @return {@link Set }<{@link String }>
     */
    Set<String> getPermission(String command);

    /**
     * 持久化命令的权限信息
     */
    void savePermissions();

    /**
     * 加载命令的权限信息
     */
    void loadPermissions();

    @Override
    default void init() {
        Context.INSTANCE.setCommandManager(this);
        loadPermissions();
        log.info("命令管理器初始化完成");
    }
}
