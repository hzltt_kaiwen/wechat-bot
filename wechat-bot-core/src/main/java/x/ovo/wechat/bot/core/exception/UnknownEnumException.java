package x.ovo.wechat.bot.core.exception;

import lombok.experimental.StandardException;

/**
 * 未知枚举异常
 *
 * @author ovo, created by 2024/07/06
 */
@StandardException
public class UnknownEnumException extends Throwable{
}
