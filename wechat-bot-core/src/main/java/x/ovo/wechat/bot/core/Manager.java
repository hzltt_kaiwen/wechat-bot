package x.ovo.wechat.bot.core;

/**
 * 管理器接口
 *
 * @author ovo, created by 2024/07/06
 */
public interface Manager {

    /**
     * 初始化，主要是将管理器注册到Context上下文
     */
    void init();

    /**
     * 获取上下文
     *
     * @return {@link Context }
     */
    default Context getContext() {
        return Context.INSTANCE;
    }

}
