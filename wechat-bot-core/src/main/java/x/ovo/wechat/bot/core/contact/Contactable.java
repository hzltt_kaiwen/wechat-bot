package x.ovo.wechat.bot.core.contact;

import x.ovo.wechat.bot.core.Context;
import x.ovo.wechat.bot.core.enums.ContactType;

import java.io.File;
import java.io.Serializable;

public interface Contactable extends Serializable {

    String getUserName();

    String getNickName();

    String getRemarkName();

    Integer getVerifyFlag();

    void format();


    default ContactType getType() {
        return ContactType.get(this);
    }

    default boolean sendMessage(String message) {
        return Context.INSTANCE.getApi().sendText(this.getUserName(), message);
    }

    default boolean sendEmoticon(String md5) {
        return Context.INSTANCE.getApi().sendEmoticon(this.getUserName(), md5);
    }

    default boolean sendImage(File file) {
        return Context.INSTANCE.getApi().sendImage(this.getUserName(), file);
    }

    default boolean sendFile(File file) {
        return Context.INSTANCE.getApi().sendFile(this.getUserName(), file);
    }

    default boolean sendVideo(File file) {
        return Context.INSTANCE.getApi().sendVideo(this.getUserName(), file);
    }

    default boolean sendVoice(File file) {
        return Context.INSTANCE.getApi().sendVoice(this.getUserName(), file);
    }
}
