package x.ovo.wechat.bot.core.message;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 图像消息
 *
 * @author ovo, created by 2024/07/06
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ImageMessage extends Message{

    /** 图片字节数组 */
    private byte[] bytes;
    /** 宽度 */
    private int width;
    /** 高度 */
    private int height;

    @Override
    public String getContent() {
        return String.format("[图片] (%dx%d %dk)", this.width, this.height, this.bytes == null ? 0 : this.bytes.length / 1024);
    }

}
