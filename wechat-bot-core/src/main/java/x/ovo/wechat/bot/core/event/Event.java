package x.ovo.wechat.bot.core.event;

import lombok.Getter;
import lombok.Setter;
import x.ovo.wechat.bot.core.Context;

import java.util.EventObject;

/**
 * 事件基类
 *
 * @author ovo, created by 2024/07/06
 */
@Setter@Getter
public abstract class Event<T> extends EventObject {

    /** 上下文 */
    private Context context = Context.INSTANCE;

    public Event(T source) {
        super(source);
    }

    @Override
    @SuppressWarnings("unchecked")
    public T getSource() {
        return (T) super.getSource();
    }

    public void fire() {
        this.context.getEventManager().fireEvent(this);
    }
}
