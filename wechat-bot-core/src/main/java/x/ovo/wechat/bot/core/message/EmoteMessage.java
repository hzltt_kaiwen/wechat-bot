package x.ovo.wechat.bot.core.message;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 表情消息
 *
 * @author ovo, created by 2024/07/06
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class EmoteMessage extends Message{

    private static final Pattern PATTERN = Pattern.compile("md5\\s?=\\s?\"(\\w+)\"");

    private String md5;

    public void setEmoticonMd5(String content) {
        Matcher matcher = PATTERN.matcher(content);
        if (matcher.find()) {
            this.md5 = matcher.group(1);
        }
    }

    @Override
    public String getContent() {
        return "[图片表情] " + this.md5;
    }
}
