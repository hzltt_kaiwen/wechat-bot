package x.ovo.wechat.bot.core.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.ToString;
import x.ovo.wechat.bot.core.exception.UnknownEnumException;

/**
 * 同步检查返回结果
 *
 * @author ovo, created by 2024/07/07
 */
public class SyncCheck {

    @Getter
    @ToString
    @RequiredArgsConstructor
    public enum RetCode {

        NORMAL(0, "正常"),
        TICK_ERROR(-14, "ticket错误"),
        PARAM_ERROR(1, "传入参数错误"),
        LOGIN_OUT(1100, "已登出微信"),
        LOGIN_OTHERWISE(1101, "已在其他地方登录"),
        MOBILE_LOGIN_OUT(1102, "cookie值无效"),
        LOGIN_FAIL(1203, "当前登录环境异常"),
        OFTEN(1205, "操作频繁"),
        UNKNOWN(9999, "未知");

        private final int code;
        private final String message;

        @SneakyThrows
        public static RetCode get(int code) {
            for (RetCode retCode : values()) {
                if (retCode.code == code) {
                    return retCode;
                }
            }
            throw new UnknownEnumException("未知的RetCode类型：" + code);
        }
    }

    @Getter
    @ToString
    @RequiredArgsConstructor
    public static enum Selector {

        NORMAL(0, "正常"),
        UNKNOW(1, "未知"),
        NEW_MSG(2, "有新消息"),
        MOD_CHAT(3, "聊天框修改"),
        MOD_CONTACT(4, "昵称修改或备注"),
        MOD_CHAT_MEMBER(5, "聊天框成员修改"),
        ADD_OR_DEL_CONTACT(6, "删除或者新增的好友信息"),
        ENTER_OR_LEAVE_CHAT(7, "进入或离开聊天界面");

        private final int code;
        private final String message;

        @SneakyThrows
        public static Selector get(int code) {
            for (Selector value : values()) {
                if (value.code == code) {
                    return value;
                }
            }
            throw new UnknownEnumException("未知的Selector类型：" + code);
        }
    }
}
