package x.ovo.wechat.bot.core.http.session;

import lombok.Data;

import java.io.Serializable;

/**
 * 同步密钥的键值对
 *
 * @author ovo, created by 2024/07/06
 */
@Data
public class Item implements Serializable {
    private String Key;
    private String Val;
}
