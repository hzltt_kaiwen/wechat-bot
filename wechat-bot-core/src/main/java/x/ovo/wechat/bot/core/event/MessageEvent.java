package x.ovo.wechat.bot.core.event;

import x.ovo.wechat.bot.core.message.Message;

/**
 * 消息事件
 *
 * @author ovo, created by 2024/07/06
 */
public class MessageEvent<T extends Message> extends Event<T> {

    public MessageEvent(T source) {
        super(source);
    }

    public String getContent() {
        return getSource().getContent();
    }
}
