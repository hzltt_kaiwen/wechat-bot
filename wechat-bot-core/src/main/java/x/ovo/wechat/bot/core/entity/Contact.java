package x.ovo.wechat.bot.core.entity;

import lombok.Data;
import x.ovo.wechat.bot.core.contact.Contactable;
import x.ovo.wechat.bot.core.util.WechatUtil;

import java.util.List;

/**
 * 联系人信息
 *
 * @author ovo, created by 2024/07/06
 */
@Data
public class Contact implements Contactable {

    private Long Uin;
    /** 用户唯一标识 */
    private String UserName;
    /** 微信昵称 */
    private String NickName;
    /** 微信头像URL */
    private String HeadImgUrl;
    /** 备注名 */
    private String RemarkName;

    /** 拼音简拼 */
    private String PYInitial;
    /** 拼音全拼 */
    private String PYQuanPin;
    /** 备注 拼音简拼 */
    private String RemarkPYInitial;
    /** 备注 拼音全拼 */
    private String RemarkPYQuanPin;
    /** 隐藏输入栏标志 */
    private Integer HideInputBarFlag;
    /** 星标朋友 */
    private Integer StarFriend;

    /** 性别 */
    private Integer Sex;
    /** 签名 */
    private String Signature;
    /** 省 */
    private String Province;
    /** 城市 */
    private String City;
    /** 别名 */
    private String Alias;
    /** 关键词 */
    private String KeyWord;
    /** verify 标志，用于确认公众号、服务号 */
    private Integer VerifyFlag;

    ///////////////////群聊相关/////////////////////
    /** 群id */
    private String EncryChatRoomId;
    /** 是所有者 */
    private Integer IsOwner;
    /** 群成员 */
    private List<Member> MemberList;
    /** 成员人数 */
    private long MemberCount;
    /** 唯一朋友 */
    private Integer UniFriend;
    /** 显示名称 */
    private String DisplayName;
    /** 聊天室所有者 */
    private String ChatRoomOwner;
    /** 所有者 uin */
    private Long OwnerUin;
    /** 聊天室 ID */
    private Long ChatRoomId;

    public void format() {
        this.NickName = WechatUtil.format(this.NickName);
        this.RemarkName = WechatUtil.format(this.RemarkName);
    }
}
