package x.ovo.wechat.bot.core.util;

import lombok.experimental.UtilityClass;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * 正则表达式工具类
 *
 * @author ovo, created by 2024/07/06
 */
@UtilityClass
public class RegexUtil {

    private static final Map<String, Pattern> PATTERNS = new HashMap<>();

    public static Matcher matcher(String reg, String text) {
        Objects.requireNonNull(reg, "正则表达式不能为空");
        Objects.requireNonNull(text, "文本不能为空");

        try {
            Pattern pattern = PATTERNS.computeIfAbsent(reg, Pattern::compile);
            return pattern.matcher(text);
        } catch (PatternSyntaxException e) {
            throw new IllegalArgumentException("无效的正则表达式: " + reg, e);
        }
    }

    public static Matcher matcher(Pattern pattern, String text) {
        Objects.requireNonNull(pattern, "正则表达式不能为空");
        Objects.requireNonNull(text, "文本不能为空");
        return pattern.matcher(text);
    }

    /**
     * 尝试匹配给定的文本，并返回第一个匹配组。
     *
     * @param reg  正则表达式字符串，需要是有效的正则表达式。
     * @param text 要匹配的文本，可以为null。
     * @return 第一个匹配组的字符串，如果没有匹配或正则表达式无效，则返回null。
     * @throws IllegalArgumentException 如果正则表达式格式无效。
     */
    public static String match(String reg, String text) {
        Matcher matcher = matcher(reg, text);
        if (matcher.find() && matcher.groupCount() >= 1) {
            return matcher.group(1);
        }
        return null;
    }

    public static String match(Pattern pattern, String text) {
        Matcher matcher = matcher(pattern, text);
        if (matcher.find() && matcher.groupCount() >= 1) {
            return matcher.group(1);
        }
        return null;
    }

    public static String match(String reg, String text, int group) {
        Matcher matcher = matcher(reg, text);
        if (matcher.find() && matcher.groupCount() >= group) {
            return matcher.group(group);
        }
        return null;
    }

    public static String match(Pattern pattern, String text, int group) {
        Matcher matcher = matcher(pattern, text);
        if (matcher.find() && matcher.groupCount() >= group) {
            return matcher.group(group);
        }
        return null;
    }
}
