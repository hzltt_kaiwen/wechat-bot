package x.ovo.wechat.bot.core.command;

import lombok.Data;
import picocli.CommandLine;
import x.ovo.wechat.bot.core.Context;
import x.ovo.wechat.bot.core.message.TextMessage;
import x.ovo.wechat.bot.core.plugin.Plugin;

/**
 * 命令执行器
 *
 * @author ovo, created by 2024/07/06
 */
@Data
@CommandLine.Command(sortOptions = false, resourceBundle = "i18n", mixinStandardHelpOptions = true)
public abstract class CommandExcutor {

    protected final Plugin plugin;
    protected final Context ctx;

    /** 用于发送的需要执行命令的那条文本消息 */
    protected TextMessage message;
    /** 命令的相关元数据，由运行时CommandLine注入，在子类实现中可能会用到 */
    @CommandLine.Spec
    protected CommandLine.Model.CommandSpec spec;

    public CommandExcutor(Plugin plugin) {
        this.plugin = plugin;
        this.ctx = plugin.getContext();
    }

    /**
     * 无权限提示
     *
     * @return {@link String }
     */
    public String noPermissionTip() {
        return "你没有权限执行此命令，请不要调皮";
    }

    /**
     * 权限检查
     *
     * @param user 执行命令的用户昵称
     * @return boolean 是否具有本插件的执行权限
     */
    public boolean hasPermission(String user) {
        return this.ctx.getCommandManager().hasPermission(this.spec.commandLine().getCommandName(), user);
    }

}
