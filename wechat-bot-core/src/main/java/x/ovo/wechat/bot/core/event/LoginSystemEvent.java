package x.ovo.wechat.bot.core.event;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 登录系统事件
 *
 * @author ovo, created by 2024/07/06
 */
public class LoginSystemEvent extends SystemEvent{
    public LoginSystemEvent(String name) {
        super(String.format("[login] 机器人 [%s] 于 %s 成功登录", name, LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))));
    }
}
