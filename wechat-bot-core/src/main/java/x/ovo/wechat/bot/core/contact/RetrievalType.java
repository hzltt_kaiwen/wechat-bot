package x.ovo.wechat.bot.core.contact;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * 检索类型
 *
 * @author ovo, created by 2024/07/06
 */
@Getter
@RequiredArgsConstructor
public enum RetrievalType {

    NICK_NAME("根据昵称查询"),
    USER_NAME("根据用户名查询"),
    REMARK_NAME("根据备注名查询");

    private final String comment;
}
