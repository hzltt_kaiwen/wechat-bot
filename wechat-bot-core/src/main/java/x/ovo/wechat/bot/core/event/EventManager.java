package x.ovo.wechat.bot.core.event;

import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import x.ovo.wechat.bot.core.Context;
import x.ovo.wechat.bot.core.Manager;
import x.ovo.wechat.bot.core.plugin.Plugin;

/**
 * 事件管理器
 *
 * @author ovo, created by 2024/07/06
 */
public interface EventManager extends Manager {

    Logger log = LoggerFactory.getLogger(EventManager.class.getSimpleName());

    /**
     * 注册一个插件及其事件监听器。
     * <p>
     * 此方法用于将一个插件和它的事件监听器绑定在一起，使得该监听器可以接收并处理特定事件。
     * 参数plugin确保不会是null，以保证注册的插件是有效的。
     *
     * @param plugin   需要注册的插件实例，不能为空。
     */
    void register(@NonNull Plugin plugin);

    /**
     * 取消注册一个事件监听器。
     * <p>
     * 此方法用于解除一个插件及其事件监听器的绑定，使得该插件不再接收任何事件。
     * 参数plugin确保不会是null，以保证要取消注册的插件是有效的。
     *
     * @param plugin 需要取消注册的插件实例，不能为空。
     */
    void unregister(@NonNull Plugin plugin);

    /**
     * 触发一个事件。
     * <p>
     * 此方法用于引发一个事件，让所有注册的监听器有机会处理这个事件。
     * 参数event确保不会是null，以保证要触发的事件是有效的。
     *
     * @param event 需要触发的事件实例，不能为空。
     */
    void fireEvent(@NonNull Event<?> event);

    @Override
    default void init() {
        Context.INSTANCE.setEventManager(this);
        log.info("事件管理器初始化完成");
    }
}
