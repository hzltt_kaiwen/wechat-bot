package x.ovo.wechat.bot.core.event;

/**
 * 系统事件
 *
 * @author ovo, created by 2024/07/06
 */
public class SystemEvent extends Event<String>{
    public SystemEvent(String source) {
        super(source);
    }
}
