package x.ovo.wechat.bot.core.message;

import lombok.Data;
import lombok.EqualsAndHashCode;
import x.ovo.wechat.bot.core.entity.Recommend;

@Data
@EqualsAndHashCode(callSuper = true)
public class VerifyMessage extends Message {

    private Recommend recommend;

}
