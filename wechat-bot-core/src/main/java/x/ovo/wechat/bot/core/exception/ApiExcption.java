package x.ovo.wechat.bot.core.exception;

import lombok.experimental.StandardException;

/**
 * API 异常
 *
 * @author ovo, created by 2024/07/06
 */
@StandardException
public class ApiExcption extends RuntimeException {
}
