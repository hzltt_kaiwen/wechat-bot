package x.ovo.wechat.bot.core.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 应用信息
 *
 * @author ovo, created by 2024/04/21
 */
@Data
public class AppInfo implements Serializable {

    /** 应用 ID */
    private String AppID;
    /** 类型 */
    private Integer Type;

}
