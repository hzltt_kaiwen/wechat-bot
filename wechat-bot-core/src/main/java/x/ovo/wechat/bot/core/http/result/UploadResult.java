package x.ovo.wechat.bot.core.http.result;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 媒体上传结果
 *
 * @author ovo, created by 2024/07/07
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class UploadResult extends ApiResult {

    private String MediaId;
    private Integer StartPos;
    private Integer CDNThumbImgHeight;
    private Integer CDNThumbImgWidth;
    private String EncryFileName;

}
