package x.ovo.wechat.bot.core.event;

/**
 * 异常事件
 *
 * @author ovo, created by 2024/07/06
 */
public class ExceptionEvent extends Event<Throwable>{

    public ExceptionEvent(Throwable source) {
        super(source);
    }
}
