package x.ovo.wechat.bot.core.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 个人名片信息
 *
 * @author ovo, created by 2024/05/02
 */
@Data
public class Recommend implements Serializable {
    /** 用户名 */
    private String UserName;
    /** 昵称 */
    private String NickName;
    /** QQ号 */
    private Integer QQNum;
    /** 省 */
    private String Province;
    /** 城市 */
    private String City;
    /** 内容 */
    private String Content;
    /** 签名 */
    private String Signature;
    /** 别名 */
    private String Alias;
    /** 添加方式的场景值 */
    private Integer Scene;
    /** 验证标志 */
    private Integer VerifyFlag;
    /** 用户属性状态 */
    private Integer AttrStatus;
    /** 性别 */
    private Integer Sex;
    /** 票据，用于加好友验证 */
    private String Ticket;
    /** 操作代码 */
    private Integer OpCode;
}