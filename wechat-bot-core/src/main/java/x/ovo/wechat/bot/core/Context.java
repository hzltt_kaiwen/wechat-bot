package x.ovo.wechat.bot.core;

import lombok.Getter;
import lombok.Setter;
import x.ovo.wechat.bot.core.command.CommandManager;
import x.ovo.wechat.bot.core.contact.ContactManager;
import x.ovo.wechat.bot.core.entity.Contact;
import x.ovo.wechat.bot.core.event.EventManager;
import x.ovo.wechat.bot.core.http.HttpEngine;
import x.ovo.wechat.bot.core.http.WechatApi;
import x.ovo.wechat.bot.core.http.session.Session;
import x.ovo.wechat.bot.core.plugin.PluginManager;

/**
 * 上下文，采用单例模式
 *
 * @author ovo, created by 2024/07/06
 */
@Getter
@SuppressWarnings("LombokSetterMayBeUsed")
public enum Context {

    INSTANCE;

    @Setter private boolean running;
    @Setter private boolean logedin;
    @Setter private boolean autoLogedin;
    @Setter
    private volatile long lastHeartbeat;

    @Setter
    private Contact me;
    @Setter
    private Contact owner;
    @Setter private WechatApi api;
    @Setter private Session session;
    @Setter private EventManager eventManager;
    @Setter private PluginManager pluginManager;
    @Setter private CommandManager commandManager;
    @Setter private ContactManager contactManager;
    @Setter
    private HttpEngine<?, ?> httpEngine;

}
