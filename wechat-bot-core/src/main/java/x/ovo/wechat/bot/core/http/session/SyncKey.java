package x.ovo.wechat.bot.core.http.session;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 同步密钥
 *
 * @author ovo, created by 2024/07/06
 */
@Data
public class SyncKey implements Serializable {

    private Integer Count;

    private List<Item> List;

    /**
     * 在synccheck接口需要用到格式化后的字符串作为参数
     *
     * @return {@link String}
     */
    @Override
    public String toString() {
        return this.List.stream()
                .map(item -> String.format("%s_%s", item.getKey(), item.getVal()))
                .collect(Collectors.joining("|"));
    }
}
