package x.ovo.wechat.bot.core.message;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * 分享消息
 *
 * @author ovo, created by 2024/07/06
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SharedMessage extends Message {

    /** 类型 */
    private Type shareType;
    /** 标题 */
    private String title;
    /** 描述 */
    private String description;
    /** 网址 */
    private String url;

    @Override
    public String getContent() {
        return String.format("[%s] %s(%s) %s", this.shareType.getName(), this.title, this.description, this.url);
    }

    @Getter
    @RequiredArgsConstructor
    public enum Type {
        UNKNOWN(-1, "未知"),
        PAGE(5, "文章"),
        IMAGE(6, "媒体文件"),
        MINI_PROGRAM(33, "文章"),
        MICRO_VIDEO(51, "微视频"),
        MUSIC(92, "音乐"),
        ;
        private final int code;
        private final String name;

        public static Type get(int code) {
            for (Type type : values()) {
                if (type.code == code) {
                    return type;
                }
            }
            return UNKNOWN;
        }
    }
}
