package x.ovo.wechat.bot.core.message;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 撤回的消息
 *
 * @author ovo, created by 2024/07/06
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class RevokeMessage extends Message {

    private static final Pattern PATTERN = Pattern.compile(";msgid&gt;(\\d+)&lt;/");
    private static final Pattern PATTERN_PLACEHOLDER = Pattern.compile("&lt;replacemsg&gt;&lt;!\\[CDATA\\[(.*)]]&gt;&lt;/replacemsg&gt;");

    private String placeholder;
    private String revokedMessageId;

    @Override
    public String getContent() {
        return String.format("[%s] %s", this.revokedMessageId, this.placeholder);
    }

    /**
     * 获取已撤销消息 ID
     *
     * @return {@link String}
     */
    public void setRevokedMessageId(String raw) {
        Matcher matcher = PATTERN.matcher(raw);
        if (matcher.find()) this.revokedMessageId = matcher.group(1);
    }

    public void setPlaceholder(String raw) {
        Matcher matcher = PATTERN_PLACEHOLDER.matcher(raw);
        if (matcher.find()) this.placeholder = matcher.group(1);
    }

}
