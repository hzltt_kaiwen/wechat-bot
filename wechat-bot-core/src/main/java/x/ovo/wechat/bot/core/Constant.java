package x.ovo.wechat.bot.core;

import java.io.File;

/**
 * 常量
 *
 * @author ovo, created by 2024/07/06
 */
public final class Constant {

    public static final String VERSION = "2.1.5";

    public static final String delimiter = "\n———————————————\n";
    public static final String GROUP_BR = ":<br/>";
    public static final String GROUP_IDENTIFY = "@@";

    public static final String APP_ID = "wx782c26e4c19acffb";
    public static final String EXTSPAM = "Go8FCIkFEokFCggwMDAwMDAwMRAGGvAESySibk50w5Wb3uTl2c2h64jVVrV7gNs06GFlWplHQbY/5FfiO++1yH4ykC" +
            "yNPWKXmco+wfQzK5R98D3so7rJ5LmGFvBLjGceleySrc3SOf2Pc1gVehzJgODeS0lDL3/I/0S2SSE98YgKleq6Uqx6ndTy9yaL9qFxJL7eiA/R" +
            "3SEfTaW1SBoSITIu+EEkXff+Pv8NHOk7N57rcGk1w0ZzRrQDkXTOXFN2iHYIzAAZPIOY45Lsh+A4slpgnDiaOvRtlQYCt97nmPLuTipOJ8Qc5p" +
            "M7ZsOsAPPrCQL7nK0I7aPrFDF0q4ziUUKettzW8MrAaiVfmbD1/VkmLNVqqZVvBCtRblXb5FHmtS8FxnqCzYP4WFvz3T0TcrOqwLX1M/DQvcHa" +
            "GGw0B0y4bZMs7lVScGBFxMj3vbFi2SRKbKhaitxHfYHAOAa0X7/MSS0RNAjdwoyGHeOepXOKY+h3iHeqCvgOH6LOifdHf/1aaZNwSkGotYnYSc" +
            "W8Yx63LnSwba7+hESrtPa/huRmB9KWvMCKbDThL/nne14hnL277EDCSocPu3rOSYjuB9gKSOdVmWsj9Dxb/iZIe+S6AiG29Esm+/eUacSba0k8" +
            "wn5HhHg9d4tIcixrxveflc8vi2/wNQGVFNsGO6tB5WF0xf/plngOvQ1/ivGV/C1Qpdhzznh0ExAVJ6dwzNg7qIEBaw+BzTJTUuRcPk92Sn6QDn" +
            "2Pu3mpONaEumacjW4w6ipPnPw+g2TfywJjeEcpSZaP4Q3YV5HG8D6UjWA4GSkBKculWpdCMadx0usMomsSS/74QgpYqcPkmamB4nVv1JxczYIT" +
            "IqItIKjD35IGKAUwAA==";
    public static final String USER_AGENT = "Mozilla/5.0 (Linux; U; UOS x86_64; zh-cn) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36 UOSBrowser/6.0.1.1001";

    /**
     * 文件相关
     *
     * @author ovo, created by 2024/07/06
     */
    public static final class Files {
        public static final String CONFIG_FILE_NAME = "config.yml";
        public static final File DATA_DIR = new File(System.getProperty("user.dir"));
        public static final File LOG_DIR = new File(DATA_DIR, "log");
        public static final File IMAGE_DIR = new File(DATA_DIR, "image");
        public static final File VIDEO_DIR = new File(DATA_DIR, "video");
        public static final File VOICE_DIR = new File(DATA_DIR, "voice");
        public static final File PLUGIN_DIR = new File(DATA_DIR, "plugin");
        public static final File CONFIG_DIR = new File(DATA_DIR, "config");

        public static final File CONFIG_FILE = new File(CONFIG_DIR, CONFIG_FILE_NAME);
        public static final File LOGIN_FILE = new File(CONFIG_DIR, "memory.card");
        public static final File GROUP_PLUGIN_FILE = new File(CONFIG_DIR, "group_plugin.json");
        public static final File COMMAND_PERMISSION_FILE = new File(CONFIG_DIR, "command_permission.json");
    }

    /**
     * url相关
     *
     * @author ovo, created by 2024/07/06
     */
    public static final class Urls {
        public final static String LOGINJS = "https://login.wx.qq.com/jslogin";
        public final static String LOGIN = "https://login.wx.qq.com/cgi-bin/mmwebwx-bin/login";
        public final static String NEWLOGINPAGE = "https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxnewloginpage";
        public static final String PUSH_LOGIN = "/cgi-bin/mmwebwx-bin/webwxpushloginurl";
        public final static String WXINIT = "/cgi-bin/mmwebwx-bin/webwxinit";
        public final static String SYNCCHECK = "/cgi-bin/mmwebwx-bin/synccheck";
        public final static String WEBWXSYNC = "/cgi-bin/mmwebwx-bin/webwxsync";
        public static final String STATUS_NOTIFY = "/cgi-bin/mmwebwx-bin/webwxstatusnotify";
        public static final String LOGOUT = "/cgi-bin/mmwebwx-bin/webwxlogout";
        public final static String GET_CONTACT = "/cgi-bin/mmwebwx-bin/webwxgetcontact";
        public final static String BATCH_GET_CONTACT = "/cgi-bin/mmwebwx-bin/webwxbatchgetcontact";
        public static final String VERIFY = "/cgi-bin/mmwebwx-bin/webwxverifyuser";
        public static final String MODIFY_CONTACT_REMARK = "/cgi-bin/mmwebwx-bin/webwxoplog";
        public static final String CREATE_GROUP = "/cgi-bin/mmwebwx-bin/webwxcreatechatroom";
        public static final String UPDATE_GROUP = "/cgi-bin/mmwebwx-bin/webwxupdatechatroom";
        public final static String GET_VIDEO = "/cgi-bin/mmwebwx-bin/webwxgetvideo";
        public final static String GET_VOICE = "/cgi-bin/mmwebwx-bin/webwxgetvoice";
        public final static String GET_MSG_IMG = "/cgi-bin/mmwebwx-bin/webwxgetmsgimg";
        public final static String REVOKE = "/cgi-bin/mmwebwx-bin/webwxrevokemsg";
        public final static String UPLOAD_FILE = "/cgi-bin/mmwebwx-bin/webwxuploadmedia";
        public final static String SEND_MESSAGE = "/cgi-bin/mmwebwx-bin/webwxsendmsg";
        public final static String SEND_EMOTICON = "/cgi-bin/mmwebwx-bin/webwxsendemoticon";
        public final static String SEND_IMAGE = "/cgi-bin/mmwebwx-bin/webwxsendmsgimg";
        public final static String SEND_VIDEO = "/cgi-bin/mmwebwx-bin/webwxsendvideomsg";
        public static final String SEND_FILE = "/cgi-bin/mmwebwx-bin/webwxsendappmsg";
        public final static String GET_ICON = "/cgi-bin/mmwebwx-bin/webwxgeticon";
    }
}
