package x.ovo.wechat.bot.core.message;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 消息留言
 *
 * @author ovo, created by 2024/07/06
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class VoiceMessage extends Message {

    /** 语音字节数组 */
    private byte[] bytes;
    /** 长度 */
    private long length;

    @Override
    public String getContent() {
        return String.format("[语音] %d s", this.length);
    }
}
