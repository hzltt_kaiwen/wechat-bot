package x.ovo.wechat.bot.core.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import x.ovo.wechat.bot.core.exception.UnknownEnumException;

/**
 * 登录方式
 *
 * @author ovo, created by 2024/07/06
 */
@Getter
@RequiredArgsConstructor
public enum LoginMode {

    SUCCESS(200, "已确认登录"),
    SCANED(201, "已扫描二维码，请确认登录"),
    FAILURE(400, "二维码失效，请重新扫描二维码"),
    WAITING(408, "等待扫码中..."),
    TIMEOUT(401, "登录超时，请重新扫描二维码");

    private final int code;
    private final String message;

    @SneakyThrows
    public static LoginMode get(int code) {
        for (LoginMode mode : values()) {
            if (mode.getCode() == code) {
                return mode;
            }
        }
        throw new UnknownEnumException("未知的登录类型: " + code);
    }
}
