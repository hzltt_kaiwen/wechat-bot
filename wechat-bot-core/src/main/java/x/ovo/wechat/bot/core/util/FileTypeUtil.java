package x.ovo.wechat.bot.core.util;

import lombok.experimental.UtilityClass;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * 文件类型工具类
 *
 * @author ovo, created by 2024/06/12
 */
@UtilityClass
public class FileTypeUtil {

    private static final String PIC = "pic";
    private static final String VIDEO = "video";
    private static final String DOC = "doc";

    // 支持的图片格式
    private static final Set<String> imageType = new HashSet<>(Arrays.asList("jpg", "jpeg", "png", "gif"));
    // 视频文件扩展名
    private static final String videoType = "mp4";

    private static String getFileExt(String filename) {
        int dotIndex = filename.lastIndexOf('.');
        if (dotIndex > 0 && dotIndex < filename.length() - 1) {
            return filename.substring(dotIndex + 1);
        }
        return "";
    }

    public static String getFileType(String filename) {
        String ext = getFileExt(filename).toLowerCase();
        if (imageType.contains(ext)) {
            return PIC;
        }
        if (ext.equals(videoType)) {
            return VIDEO;
        }
        return DOC;
    }

}
