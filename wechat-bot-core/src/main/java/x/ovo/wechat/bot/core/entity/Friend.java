package x.ovo.wechat.bot.core.entity;

public class Friend extends Contact {

    /** 星标朋友 */
    private Integer StarFriend;

    /** 性别 */
    private Integer Sex;
    /** 签名 */
    private String Signature;
    /** 省 */
    private String Province;
    /** 城市 */
    private String City;
    /** 别名 */
    private String Alias;
    /** 关键词 */
    private String KeyWord;
}
