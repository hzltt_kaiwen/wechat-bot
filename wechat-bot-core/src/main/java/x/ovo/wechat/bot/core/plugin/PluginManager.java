package x.ovo.wechat.bot.core.plugin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import x.ovo.wechat.bot.core.Context;
import x.ovo.wechat.bot.core.Manager;

import java.io.File;
import java.util.Set;

/**
 * 插件管理接口，提供插件的加载、卸载、重载以及启用、禁用等功能。
 * 还包括查询插件状态和限制信息，以及保存限制配置的功能。
 *
 * @author ovo, created by 2024/07/06
 */
public interface PluginManager extends Manager {

    Logger log = LoggerFactory.getLogger("PluginManager");

    /**
     * 加载指定名称的插件。
     *
     * @param name 插件的唯一标识符。
     */
    void load(String name);

    /**
     * 加载指定文件的插件。
     *
     * @param file 插件文件。
     */
    void load(File file);

    /**
     * 卸载指定名称的插件。
     *
     * @param name 插件的唯一标识符。
     */
    void unload(String name);

    /**
     * 卸载指定插件对象。
     *
     * @param plugin 插件对象。
     */
    void unload(Plugin plugin);

    /**
     * 启用指定名称的插件。
     *
     * @param name 插件的唯一标识符。
     * @param from 启用插件的来源。
     */
    void enable(String name, String from);

    /**
     * 禁用指定名称的插件。
     *
     * @param name 插件的唯一标识符。
     * @param from 禁用插件的来源。
     */
    void disable(String name, String from);

    /**
     * 检查指定插件是否受到限制。
     *
     * @param name 插件的唯一标识符。
     * @param from 消息的来源。
     * @return 如果插件受到限制，则返回true；否则返回false。
     */
    boolean isLimit(String name, String from);

    /**
     * 获取指定名称的插件对象。
     *
     * @param name 插件的唯一标识符。
     * @return 插件对象，如果不存在则返回null。
     */
    Plugin get(String name);

    /**
     * 列出所有已加载的插件。
     *
     * @return 包含所有插件的集合。
     */
    Set<Plugin> list();

    /**
     * 列出指定消息来源的所有可用已加载插件。
     *
     * @param from 消息的来源。
     * @return 包含指定消息来源可用插件的集合。
     */
    Set<Plugin> list(String from);

    /**
     * 保存插件限制配置。
     */
    void saveLimitConfig();

    @Override
    default void init() {
        Context.INSTANCE.setPluginManager(this);
        log.info("插件管理器初始化完成");
    }
}
