package x.ovo.wechat.bot.core.entity;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * 批量联系人查询
 *
 * @author ovo, created by 2024/07/07
 */
@Data
@Builder
public class BatchContactQuery implements Serializable {
    /** 用户名 */
    private String UserName;
    /** 群组 ID */
    private String EncryChatRoomId;
}
