package x.ovo.wechat.bot.core.http.result;

import lombok.Data;

import java.io.Serializable;


/**
 * API 请求结果
 *
 * @author ovo on 2024/07/16.
 */
@Data
public class ApiResult implements Serializable {

    protected BaseResponse BaseResponse;

    public boolean success() {
        return this.BaseResponse.getRet() == 0;
    }

    public boolean failure() {
        return !this.success();
    }

}
