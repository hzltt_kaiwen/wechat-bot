package x.ovo.wechat.bot.core.http;

import lombok.Builder;
import lombok.Data;

/**
 * http 引擎配置
 *
 * @author ovo, created by 2024/07/06
 */
@Data@Builder
public class EngineConfig {

    /** 连接超时 */
    private int connectionTimeout;
    /** 读取超时 */
    private int readTimeout;
    /** 写入超时 */
    private int writeTimeout;
    /** 禁用缓存 */
    private boolean disableCache;

    /**
     * 默认配置<p>
     * 连接超时：60s，读取超时：120s，写入超时：60s，禁用缓存：false
     *
     * @return {@link EngineConfig }
     */
    public static EngineConfig defaultConfig() {
        return EngineConfig.builder()
                .connectionTimeout(60)
                .readTimeout(120)
                .writeTimeout(60)
                .disableCache(false)
                .build();
    }
}
