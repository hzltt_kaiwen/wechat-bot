package x.ovo.wechat.bot.core.message;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 二维码收款到账消息
 *
 * @author ovo, created by 2024/07/06
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PayMessage extends Message {

    private static final Pattern AMOUNT_PATTERN = Pattern.compile("微信支付收款([-+]?[0-9]*\\.?[0-9]+)元");
    private static final Pattern REMARK_PATTERN = Pattern.compile("<br/>付款方备注(.+?)<br/>");

    /** 金额 */
    private double amount;
    /** 备注 */
    private String remark;

    /*
    提取金额信息
     */
    public double extractAmount() {
        Matcher matcher = AMOUNT_PATTERN.matcher(super.content);
        if (matcher.find()) this.amount = Double.parseDouble(matcher.group(1));
        return this.amount;
    }


    /**
     * 提取备注信息
     */
    public String extractNotes() {
        Matcher matcher = REMARK_PATTERN.matcher(super.content);
        if (matcher.find()) this.remark = matcher.group(1);
        return this.remark;
    }

    @Override
    public String getContent() {
        return String.format("[收款信息] 金额: %f 元, 备注: %s", this.extractAmount(), this.extractNotes());
    }
}
