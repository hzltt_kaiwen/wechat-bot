package x.ovo.wechat.bot.core.util;

import lombok.experimental.UtilityClass;

import java.util.Arrays;

/**
 * 命令 util
 *
 * @author ovo on 2024/07/24.
 */
@UtilityClass
public class CommandUtil {

    /**
     * 获取命令
     *
     * @param message 消息
     * @return {@link String }
     */
    public static String getCommand(String message) {
        return WechatUtil.format(message).split(" ")[0];
    }

    /**
     * 获取参数
     *
     * @param message 消息
     * @return {@link String[] }
     */
    public static String[] getArgs(String message) {
        return Arrays.stream(WechatUtil.format(message).split(" "))
                .skip(1)
                .toArray(String[]::new);
    }

}
