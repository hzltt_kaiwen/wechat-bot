package x.ovo.wechat.bot.core.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import x.ovo.wechat.bot.core.exception.UnknownEnumException;

/**
 * 消息类型
 *
 * @author ovo, created by 2024/07/06
 */
@Getter
@RequiredArgsConstructor
public enum MessageType {

    ALL(0, "全部消息"),
    TEXT(1, "文本消息"),
    IMAGE(3, "图片消息"),
    FILE(6, "文件消息"),
    VOICE(34, "语音消息"),
    VERIFY(37, "好友请求消息"),
    POSSIBLE_FRIEND(40, "可能为好友消息"),
    SHARED_REQ(42, "共享名片消息"),
    VIDEO(43, "视频消息"),
    EMOTICON(47, "表情消息"),
    POSITION(48, "位置消息"),
    SHARING(49, "分享链接"),
    VOIP(50, "VoIP通话消息"),
    STATUSNOTIFY(51, "状态通知消息"),
    VOIPNOTIFY(52, "VoIP通话通知"),
    VOIPINVITE(53, "VoIP邀请消息"),
    MICROVIDEO(62, "小视频消息"),
    SYSNOTIVET(9999, "系统通知消息"),
    SYSTEM(10000, "系统消息"),
    RECALLED(10002, "撤回消息"),
    ;

    private final int code;
    private final String name;

    @SneakyThrows
    public static MessageType get(int code) {
        for (MessageType msgType : MessageType.values()) {
            if (msgType.getCode() == code) {
                return msgType;
            }
        }
        throw new UnknownEnumException("未知消息类型: " + code);
    }
}
