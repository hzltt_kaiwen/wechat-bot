package x.ovo.wechat.bot.core.message;

import lombok.Data;
import x.ovo.wechat.bot.core.Constant;
import x.ovo.wechat.bot.core.entity.AppInfo;
import x.ovo.wechat.bot.core.entity.Recommend;

import java.io.Serializable;

/**
 * 微信原始消息
 *
 * @author ovo, created by 2024/05/01
 */
@Data
public class RawMessage implements Serializable {

    /** 消息 ID */
    private Long MsgId;
    /** 发送用户名 */
    private String FromUserName;
    /** 接收用户名 */
    private String ToUserName;
    /** 消息类型 */
    private Integer MsgType;
    /** 内容 */
    private String Content;
    /** 状态 */
    private Integer Status;
    /** 图片状态 */
    private Integer ImgStatus;
    /** 创建时间 */
    private Long CreateTime;
    /** 语音长度 */
    private Long VoiceLength;
    /** 视频长度 */
    private Long PlayLength;
    /** 文件名 */
    private String FileName;
    /** 文件大小 */
    private String FileSize;
    /** 媒体 ID */
    private String MediaId;
    /** 网址 */
    private String Url;
    /** 应用消息类型 */
    private Integer AppMsgType;
    /** 状态通知代码 */
    private Integer StatusNotifyCode;
    /** 状态通知用户名 */
    private String StatusNotifyUserName;
    /** 推荐信息 */
    private Recommend RecommendInfo;
    /** 前进标志 */
    private Integer ForwardFlag;
    /** 应用信息 */
    private AppInfo AppInfo;
    /** 具有产品 ID */
    private Integer HasProductId;
    /** 票 */
    private String Ticket;
    /** IMG高度 */
    private Integer ImgHeight;
    /** img 宽度 */
    private Integer ImgWidth;
    /** 子消息类型 */
    private Integer SubMsgType;
    /** 新消息 ID */
    private Long NewMsgId;
    /** ORI内容 */
    private String OriContent;
    /** Encry 文件名 */
    private String EncryFileName;


    /**
     * 是否是群聊消息
     *
     * @return 返回是否是群组消息
     */
    public boolean isGroup() {
        return FromUserName.contains(Constant.GROUP_IDENTIFY) || ToUserName.contains(Constant.GROUP_IDENTIFY);
    }

}
