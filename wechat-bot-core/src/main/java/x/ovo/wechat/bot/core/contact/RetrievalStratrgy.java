package x.ovo.wechat.bot.core.contact;

import x.ovo.wechat.bot.core.entity.Contact;

import java.util.Iterator;

/**
 * 检索策略
 *
 * @author ovo, created by 2024/07/06
 */
public abstract class RetrievalStratrgy {

    /**
     * 获取检索类型。
     *
     * @return 返回检索类型，具体类型由实现决定。
     */
    public abstract RetrievalType getType();

    /**
     * 根据键值从迭代器中获取联系人。
     *
     * @param iterator 联系人的迭代器，用于遍历联系人集合。
     * @param key      要检索的联系人的键值。
     * @return 返回与键值匹配的 {@link Contact } ，如果没有找到，则返回null。
     */
    public abstract Contactable get(Iterator<? extends Contactable> iterator, String key);

    /**
     * 从迭代器中移除与键值匹配的联系人。
     *
     * @param iterator 联系人的迭代器，用于遍历并移除联系人。
     * @param key      要移除的联系人的键值。
     * @return 如果成功移除了匹配的联系人，则返回true；否则返回false。
     */
    public abstract boolean remove(Iterator<? extends Contactable> iterator, String key);

    /**
     * 初始化检索策略，将自己注册到联系人管理器的检索策略映射中。
     */
    public void init() {
        ContactManager.STRATRGY_MAP.put(this.getType(), this);
    }
}
