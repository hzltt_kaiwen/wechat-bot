package x.ovo.wechat.bot.core.entity;

import java.util.List;

public class Group extends Contact {

    ///////////////////群聊相关/////////////////////
    /** 群id */
    private String EncryChatRoomId;
    /** 是所有者 */
    private Integer IsOwner;
    /** 群成员 */
    private List<Member> MemberList;
    /** 成员人数 */
    private long MemberCount;
    /** 唯一朋友 */
    private Integer UniFriend;
    /** 显示名称 */
    private String DisplayName;
    /** 聊天室所有者 */
    private String ChatRoomOwner;
    /** 所有者 uin */
    private Long OwnerUin;
    /** 聊天室 ID */
    private Long ChatRoomId;

}
