package x.ovo.wechat.bot.core.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * Profile信息类，用于存储用户的基本信息。
 *
 * @author ovo, created by 2024/04/21
 */
@Data
public class Profile implements Serializable {

    /** 位标志，用于表示用户的不同状态或属性 */
    private Integer BitFlag;
    /** 用户名，一个封装了用户名信息的内部类 */
    private Buff UserName;
    /** 昵称，用户设置的昵称 */
    private Buff NickName;
    /** 绑定的UIN，即用户在某个系统中的唯一标识符 */
    private Integer BindUin;
    /** 绑定的邮箱地址，一个封装了邮箱地址信息的内部类 */
    private Buff BindEmail;
    /** 绑定的手机号码，一个封装了手机号码信息的内部类 */
    private Buff BindMobile;
    /** 用户状态，通常用于表示用户是否在线等状态 */
    private Integer Status;
    /** 用户性别，通常用0、1、2表示未知、男、女 */
    private Integer Sex;
    /** 个人说明，用户自己填写的个人介绍或说明 */
    private Integer PersonalCard;
    /** 别名，用户可以设置的别名，用于好友备注等 */
    private String Alias;
    /** 头像更新标志，用于表示头像是否已更新 */
    private Integer HeadImgUpdateFlag;
    /** 头像URL，用户头像的网络地址 */
    private String HeadImgUrl;
    /** 个性签名，用户自己设置的个性签名或标语 */
    private String Signature;

    /**
     * 内部类，用于封装用户名、邮箱地址和手机号码等信息。
     * 其中Buff字段用于存储实际的用户名、邮箱或手机号码。
     */
    @Data
    static class Buff {
        String Buff;
    }

}
