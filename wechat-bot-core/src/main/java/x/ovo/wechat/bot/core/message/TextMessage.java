package x.ovo.wechat.bot.core.message;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 文本消息
 *
 * @author ovo, created by 2024/07/06
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TextMessage extends Message {

    /** 是否是@我的消息 */
    private boolean atMe = false;

}
