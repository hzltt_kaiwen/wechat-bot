package x.ovo.wechat.bot.core.exception;

import lombok.experimental.StandardException;

/**
 * 同步检查异常
 *
 * @author ovo, created by 2024/07/12
 */
@StandardException
public class SyncCheckException extends RuntimeException{
}
