package x.ovo.wechat.bot.core.http.result;

import lombok.Data;
import x.ovo.wechat.bot.core.entity.Contact;
import x.ovo.wechat.bot.core.entity.Profile;
import x.ovo.wechat.bot.core.http.session.SyncKey;
import x.ovo.wechat.bot.core.message.RawMessage;

import java.io.Serializable;
import java.util.List;

/**
 * WebSyncResult 类用于表示网页端同步结果的数据模型，包含了一系列同步操作的结果信息，如消息、联系人变更等。
 *
 * @author ovo, created by 2024/05/03
 */
@Data
public class WebSyncResult implements Serializable {
    /** 添加的消息数量 */
    private Integer AddMsgCount;
    /** 添加的消息列表 */
    private List<RawMessage> AddMsgList;
    /** 修改的联系人数量 */
    private Integer ModContactCount;
    /** 修改的联系人列表 */
    private List<Contact> ModContactList;

    /** 删除的联系人数量 */
    private Integer DelContactCount;
    /** 删除的联系人列表 */
    private List<Contact> DelContactList;

    /** 修改的聊天室成员数量 */
    private Integer ModChatRoomMemberCount;
    /** 修改的聊天室成员列表 */
    private List<Contact> ModChatRoomMemberList;

    /** 用户个人资料信息 */
    private Profile Profile;
    /** 继续标志，用于指示是否继续同步 */
    private Integer ContinueFlag;
    /** 会话键，用于标识特定的会话 */
    private String SKey;
    /** 同步键，包含需要同步的消息和联系人的标识信息 */
    private SyncKey SyncKey;
    /** 同步检查键，用于检查是否需要进行下一次同步 */
    private SyncKey SyncCheckKey;
}

