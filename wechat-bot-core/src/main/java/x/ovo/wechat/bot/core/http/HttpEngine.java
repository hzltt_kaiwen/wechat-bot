package x.ovo.wechat.bot.core.http;

import x.ovo.wechat.bot.core.exception.ApiExcption;
import x.ovo.wechat.bot.core.http.request.ApiRequest;

import java.util.function.Function;

/**
 * HttpEngine接口定义了与HTTP请求相关的操作，用于发送HTTP请求并处理响应。
 * <p>
 * 它是一个通用接口，允许具体的HTTP客户端实现（如OkHttp、HttpURLConnection等）来提供HTTP通信能力。
 * 该接口的主要作用是抽象出HTTP请求的发送和响应的处理过程，使得上层代码可以通过一个统一的接口来交互与HTTP服务，
 * 而不需要关心具体的HTTP客户端实现细节。
 *
 * @author ovo, created by 2024/07/06
 */
public interface HttpEngine<E, R> {

    /**
     * 使用配置信息初始化HTTP引擎。
     * <p>
     * 该方法在使用HTTP引擎发送请求之前必须被调用，以配置HTTP引擎的行为，如设置代理、超时时间等。
     *
     * @param config 引擎的配置信息，包含了初始化HTTP引擎所需的各种参数。
     */
    void init(EngineConfig config);

    /**
     * 获取底层的原始HTTP引擎实例。
     * <p>
     * 该方法允许调用者直接操作底层的HTTP引擎，以便进行更底层的配置或操作，如设置请求头、配置连接池等。
     * 但是，通常情况下，应该尽量使用HttpEngine提供的抽象方法，以保持代码的可移植性和可维护性。
     *
     * @return 底层的原始HTTP引擎实例。
     */
    E getRawEngine();

    /**
     * 发送HTTP请求并处理响应。
     * <p>
     * 该方法是HttpEngine的核心方法，它负责发送HTTP请求，并使用提供的响应处理器处理响应。
     * 这种设计允许灵活地处理不同的响应格式，如JSON、XML等。
     *
     * @param request         HTTP请求对象，包含了请求的URL、方法、参数等信息。
     * @param responseHandler 响应处理器，用于将服务器响应转换为特定的Java类型。
     * @param <T>             响应处理器返回的Java类型的泛型参数。
     * @return 经过响应处理器处理后的结果。
     * @throws ApiExcption 如果发送HTTP请求或处理响应过程中发生错误，则抛出ApiExcption。
     */
    <S, T> T execute(ApiRequest<T> request, Function<S, T> responseHandler, Class<?> clazz) throws ApiExcption;

    R convert(ApiRequest<?> request);
}
